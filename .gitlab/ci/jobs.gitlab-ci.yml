include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml

# ======================================================================================================================
# Runners
# ======================================================================================================================
.docker_credentials: &docker_credentials
  - |
    mkdir -p $HOME/.docker
    cat <<- EOF > $HOME/.docker/config.json
    {
      "auths": {
        "$DOCKER_REGISTRY": {
          "auth": "$(echo -n $DOCKER_CREDENTIALS | base64)"
        }
      }
    }
    EOF

.gem_cache: &gem_cache
  key:
    files:
      - Gemfile.lock
  paths:
    - vendor/bundle
  policy: pull

.tf_cache: &tf_cache
  key: tf-${CI_COMMIT_REF_SLUG}-${TF_STATE_NAME}
  paths:
    - terraform/.terraform
  policy: pull

.codacy_cache: &codacy_cache
  key: codacy-coverage-reporter-$CODACY_VERSION
  paths:
    - codacy-coverage-reporter-$CODACY_VERSION
  policy: pull

.ruby_runner:
  image: registry.gitlab.com/dependabot-gitlab/dependabot/ruby:2.7.4-debian-11-latest
  variables:
    BUNDLE_PATH: vendor/bundle
    BUNDLE_FROZEN: "true"
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - bundle install
  cache:
    - *gem_cache

.terraform_runner:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  variables:
    GOOGLE_CLOUD_KEYFILE_JSON: $KEYFILE_DEPENDABOT_GITLAB
    TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}
    TF_VAR_dependabot_host: $DEPENDABOT_HOST
    TF_VAR_gitlab_access_token: $GITLAB_ACCESS_TOKEN
    TF_VAR_github_access_token: $GITHUB_ACCESS_TOKEN
    TF_VAR_image_tag: $CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
    TF_VAR_environment: $TF_STATE_NAME
    CACHE_FALLBACK_KEY: tf-${CI_DEFAULT_BRANCH}-${TF_STATE_NAME}
  before_script:
    - cd terraform
  cache: *tf_cache

.buildkit_runner:
  image:
    name: moby/buildkit:v0.9.1
    entrypoint: [""]
  variables:
    DOCKER_CREDENTIALS: $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
    DOCKER_REGISTRY: registry.gitlab.com
    PUSH: "true"
  before_script:
    - *docker_credentials
  script:
    - script/build.sh
  retry: 2

.docker_runner:
  image: docker:20.10.5
  services:
    - docker:20.10.5-dind
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: /certs
    DOCKER_CERT_PATH: /certs/client
    DOCKER_TLS_VERIFY: 1
    DOCKER_BUILDKIT: 1
    DOCKER_CREDENTIALS: $DOCKERHUB_USERNAME:$DOCKERHUB_PASSWORD
    DOCKER_REGISTRY: https://index.docker.io/v1/
  before_script:
    - *docker_credentials
    - apk -qq update && apk -qq add grep
    - while ! test -f "$DOCKER_CERT_PATH/ca.pem"; do sleep 2; done

# ======================================================================================================================
# Global
# ======================================================================================================================
stages:
  - static analysis
  - build
  - test
  - report
  - release
  - deploy

variables:
  CURRENT_TAG: $CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
  LATEST_TAG: $CI_COMMIT_REF_SLUG-latest
  CODACY_VERSION: "12.2.0"
  DEPENDENCY_SCANNING_DISABLED: "true"
  SAST_DISABLED: "true"
  CONTAINER_SCANNING_DISABLED: "true"
  CODE_QUALITY_DISABLED: "true"
  TF_STATE_NAME: production

# ======================================================================================================================
# Jobs
# ======================================================================================================================
.cache_dependencies:
  stage: .pre
  extends: .ruby_runner
  script:
    - ./script/download-codacy.sh
  cache:
    - <<: *gem_cache
      policy: pull-push
    - <<: *codacy_cache
      policy: pull-push

.tf_init:
  stage: .pre
  extends: .terraform_runner
  script:
    - gitlab-terraform init
  cache:
    <<: *tf_cache
    policy: pull-push

.build_app_image:
  stage: build
  extends: .buildkit_runner
  needs: []
  variables:
    DOCKER_CONTEXT: "."

.build_mock_image:
  stage: build
  extends: .buildkit_runner
  needs: []
  variables:
    DOCKER_CONTEXT: spec/fixture/gitlab
    DOCKER_IMAGE: gitlab-mock

.build_ruby_image:
  stage: build
  extends: .buildkit_runner
  needs: []
  variables:
    DOCKER_CONTEXT: .gitlab/ci
    DOCKER_IMAGE: ruby
    PUSH: "false"

.tf_build:
  extends: .terraform_runner
  stage: build
  resource_group: $TF_STATE_NAME
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  artifacts:
    reports:
      terraform: terraform/plan.json

.rubocop:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec rubocop --parallel --color

.reek:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .

.brakeman:
  stage: static analysis
  extends: brakeman-sast

.bundle_audit:
  stage: static analysis
  extends: bundler-audit-dependency_scanning

.dependency_scan:
  stage: static analysis
  extends: gemnasium-dependency_scanning
  variables:
    DS_REMEDIATE: "false"

.tf_fmt:
  stage: static analysis
  extends: .terraform_runner
  script:
    - gitlab-terraform fmt

.tf_validate:
  stage: static analysis
  extends: .terraform_runner
  script:
    - gitlab-terraform validate

.rspec:
  stage: test
  extends: .ruby_runner
  services:
    - name: bitnami/redis:6.2-debian-10
      alias: redis
    - name: bitnami/mongodb:4.4-debian-10
      alias: mongodb
  variables:
    REDIS_URL: redis://redis:6379
    REDIS_PASSWORD: $REDIS_PASSWORD
    MONGODB_URL: mongodb:27017
    COVERAGE: "true"
    MAX_ROWS: 5
    OUTPUT_STYLE: block
  script:
    - bundle exec rspec --format documentation --format RspecJunitFormatter --out tmp/rspec.xml
  after_script:
    - ./codacy-coverage-reporter-$CODACY_VERSION report -r coverage/coverage.xml
  cache:
    - *gem_cache
    - *codacy_cache
  artifacts:
    reports:
      cobertura: coverage/coverage.xml
      junit: tmp/rspec.xml
    paths:
      - coverage/.resultset.json
      - reports/allure-results
    expire_in: 7 days

.e2e:
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE:$CURRENT_TAG
    entrypoint: [""]
  services:
    - name: $CI_REGISTRY_IMAGE/gitlab-mock:$CURRENT_TAG
      alias: gitlab
  variables:
    SETTINGS__GITLAB_URL: http://gitlab:4567
    SETTINGS__GITLAB_ACCESS_TOKEN: e2e-test
    SETTINGS__STANDALONE: "true"
    SETTINGS__LOG_LEVEL: "debug"
    RAILS_ENV: production
    GIT_STRATEGY: none
  script:
    - cd /home/dependabot/app && bundle exec rake 'dependabot:update[test-repo,bundler,/]'

.container_scanning:
  stage: test
  extends: container_scanning
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA

.coverage:
  stage: report
  extends: .ruby_runner
  variables:
    NO_COLOR: 1
    MAX_ROWS: 1
    OUTPUT_STYLE: block
  script:
    - bundle exec rake coverage
.coverage_line:
  extends: .coverage
  coverage: /^COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/
.coverage_branch:
  extends: .coverage
  coverage: /^BRANCH COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/

.allure_report:
  stage: report
  image:
    name: andrcuns/allure-report-publisher:0.3.6
    entrypoint: [""]
  variables:
    GITLAB_AUTH_TOKEN: $GITLAB_ACCESS_TOKEN
    ALLURE_JOB_NAME: rspec
    GOOGLE_CLOUD_KEYFILE_JSON: $KEYFILE_TEST_REPORTS
  script:
    - |
      allure-report-publisher upload gcs \
        --results-glob="reports/allure-results/*" \
        --bucket="allure-test-reports" \
        --prefix="dependabot-gitlab/$CI_COMMIT_REF_SLUG" \
        --update-pr="comment" \
        --copy-latest \
        --color

.release_image:
  stage: release
  extends: .docker_runner
  variables:
    RELEASE_IMAGE: docker.io/andrcuns/dependabot-gitlab
  script:
    - export RELEASE_VERSION=$(echo $CI_COMMIT_TAG | grep -oP 'v\K[0-9.]+')
    - docker pull $CI_REGISTRY_IMAGE:$CURRENT_TAG
    - docker tag $CI_REGISTRY_IMAGE:$CURRENT_TAG $RELEASE_IMAGE:$RELEASE_VERSION
    - docker tag $CI_REGISTRY_IMAGE:$CURRENT_TAG $RELEASE_IMAGE:latest
    - docker push $RELEASE_IMAGE:$RELEASE_VERSION && docker push $RELEASE_IMAGE:latest

.release_ruby_image:
  stage: release
  extends: .build_ruby_image
  variables:
    CURRENT_TAG: 2.7.4-debian-11-$CI_COMMIT_SHORT_SHA
    LATEST_TAG: 2.7.4-debian-11-latest
    PUSH: "true"

.gitlab_release:
  stage: release
  extends: .ruby_runner
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:gitlab[$CI_COMMIT_TAG]"

.update_chart:
  extends: .ruby_runner
  stage: release
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:chart[$CI_COMMIT_TAG]"

.update_standalone:
  extends: .ruby_runner
  stage: release
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:standalone[$CI_COMMIT_TAG]"

.tf_deploy:
  extends: .terraform_runner
  stage: deploy
  resource_group: $TF_STATE_NAME
  script:
    - gitlab-terraform plan
    - gitlab-terraform apply

.tf_stop:
  extends: .terraform_runner
  stage: deploy
  resource_group: $TF_STATE_NAME
  script:
    - gitlab-terraform init
    - gitlab-terraform plan
    - gitlab-terraform destroy
